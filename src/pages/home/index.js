/* global fetch */

import UrlQueries from '@/library/url-queries'

import ImageCard from '@/components/image-card'
import AudioBox from '@/components/audio-box'

import './home.less'

export default {
  name: 'Home',
  components: {
    'image-card': ImageCard,
    'audio-box': AudioBox
  },
  data () {
    return {
      urlData: UrlQueries('https://images-api.nasa.gov/search?q=apollo%2011&media_type=image'),
      items: [],
      audio: [],
      more: '',
      hits: 0
    }
  },
  methods: {
    fetchData (url) {
      const dataImage = fetch(url)
        .then(resp => resp.json())
        .then(json => {
          this.items = json.collection.items
          this.more = json.collection.links
          this.hits = json.collection.metadata.total_hits
        })

      const audioUrl = UrlQueries(this.url)
        .queriesAdd({ media_type: 'audio' })
        .getQueryUrl()

      const dataAudio = fetch(audioUrl)
        .then(resp => resp.json())
        .then(json => {
          this.audio = json.collection.items.map(item => ({
            description: item.data[0].title,
            audio: item.href
          }))
        })

      Promise.all([dataImage, dataAudio])
    },
    backToTop () {
      const currentScroll = document.documentElement.scrollTop || document.body.scrollTop
      if (currentScroll > 0) {
        window.requestAnimationFrame(this.backToTop)
        window.scrollTo(0, currentScroll - (currentScroll / 5))
      }
    }
  },
  computed: {
    url: {
      get () { return this.urlData.getQueryUrl() },
      set (query) {
        if (typeof query === 'string') this.urlData.queriesAdd({ q: query })
        else this.urlData.queriesAdd(query)
      }
    }
  },
  watch: {
    url () {
      this.fetchData(this.url)
    }
  },
  render (vc) {
    return vc('main', {
      attrs: {
        id: 'Home'
      }
    }, [
      vc('header', {
        class: 'jumbotron'
      }, ['Library']),
      vc('input', {
        class: 'text-large',
        placeholder: 'Search...',
        on: {
          change: (e) => {
            this.url = e.target.value
          }
        }
      }),
      vc('audio-box', {
        props: {
          audio: this.audio
        }
      }),
      vc('section', {
        class: 'image-tiles'
      },
        (this.items || []).map(item => {
          return vc('image-card', {
            props: {
              item
            }
          })
        })
      ),
      vc('footer', {
        class: this.more ? 'top-bar' : '',
        style: this.more.length === 1
          ? 'justify-content: end'
          : ''
      }, [
        (this.more || []).map(dir => vc('div', {
          class: 'btn-dir',
          on: {
            click: () => {
              this.fetchData(dir.href)
              this.backToTop()
            }
          }
        }, dir.prompt))
      ])
    ])
  }
}
