/* eslint-env jest */

import UrlQueries from './'

import MD from './mock.data.js'

/* QUOKKA */
/* eslint-disable */

const test = UrlQueries('Hello', ['World', 'wide', 'web'])

test
  .queriesAdd(['Bish', 'Bash', 'Bosh']) // Add and array of strings
  .queriesRemove(['Bish', 'Bosh']) // Remove key names as a String or Array of strings
  .queriesAdd({ 'good morning': 'morning', fizz: 'bang' }) // Add an Object of Strign KVP's
  .queriesAdd({ 'good morning': '???', fizz: '!!!' })
  .queriesRemove(['Bish', 'good morning'])
  .getQueryUrl() /*?*/

// const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=flickrcb&tags=dogs%20in%20hats'
// const check = UrlQueries(url).queryParts

// check /*?*/

/* eslint-enable */
/* QUOKKA */

describe('Tesing the Query Build function', () => {
  const test = UrlQueries(MD.url, MD.queries)

  it('Constructs factory', () => {
    expect(typeof test === 'object').toBeTruthy()
  })
  it('Checks for base URL', () => {
    expect(() => UrlQueries()).toThrowError('Base URL Required')
  })
  it('Returns string', () => {
    expect(typeof test.getQueryUrl() === 'string').toBeTruthy()
    expect(test.getQueryUrl()).toMatch(MD.url)
    expect(test.getQueryUrl()).toMatch(MD.queries[0])
  })
  it('Accepts query strings / arrays', () => {
    expect(typeof test.getQueryUrl() === 'string').toBeTruthy()
  })
  it('Replaces spaces with %20', () => {
    const str = 'Hello old friend'
    const check = UrlQueries(MD.url, [str, str])

    expect(check.getQueryUrl()).not.toMatch(/[ ]/)
    expect(check.getQueryUrl()).toMatch(/%20/)
  })
  it('Appends first query with ?', () => {
    expect(test.getQueryUrl()).toMatch(/\?/)
    expect(/\?/.exec(test.getQueryUrl()).length).toBe(1)
  })
  it('Appends subsequent queries with &', () => {
    expect(test.getQueryUrl()).toMatch(/&/g)
    expect(test.getQueryUrl().match(/&/g).length).toBe(MD.queries.length - 1)
  })
  it('Adds queries', () => {
    const arr = ['God', 'Save', 'The', 'Queen']
    const check = UrlQueries(MD.url, MD.queries).queriesAdd(arr).getQueryUrl()

    expect(check).toMatch(/&/)
    expect(check.match(/&/g).length).toBe((MD.queries.length - 1) + arr.length)
    expect(check.match(/(God|Save|The|Queen)/g).length).toBe(arr.length)
  })
  it('Removes queries', () => {
    const check = UrlQueries(MD.url, MD.queries)
      .queriesRemove(MD.queries.slice(0, -1))
      .queryParts

    const objectArray = check.map(c => Object.keys(c).pop())
    expect(objectArray).toEqual([MD.queries.pop()])
  })

  describe('Tesing key value pairs', () => {
    it('Converts arrays to arrays of KVP Objects', () => {
      const check = UrlQueries(MD.url, MD.queries)
        .queriesAdd(['Bish', 'Bash', 'Bosh'])
        .queriesRemove(['Bish', 'Bosh'])
        .queryParts

      expect(check).toEqual(expect.arrayContaining([{ bash: undefined }]))
    })

    it('Splits KVP Object into an array of KVP Objects', () => {
      const check = UrlQueries(MD.url, MD.queries)
        .queriesAdd(['Bish', 'Bash', 'Bosh'])
        .queriesAdd({ 'good morning': 'morning', fizz: 'bang' })
        .queriesRemove(['Bish', 'Bosh', 'fizz'])
        .queryParts

      expect(check).toEqual(expect.arrayContaining([{ 'good morning': 'morning' }]))
      expect(check).not.toEqual(expect.arrayContaining([{ 'fizz': 'bang' }]))
    })
  })

  it('Generates string from KVP', () => {
    const check = UrlQueries(MD.url, MD.queries)
      .queriesAdd(['Bish', 'Bash', 'Bosh'])
      .queriesAdd({ 'good morning': 'morning', fizz: 'bang' })
      .queriesRemove(['Bish', 'Bosh', 'fizz'])
      .getQueryUrl()

    expect(check).toMatch(/(Bash|good morning)/g)
  })

  it('Generates object from URL query string', () => {
    const url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=flickrcb&tags=dogs%20in%20hats'
    const check = UrlQueries(url).queryParts
    expect(check).toEqual(expect.arrayContaining([
      { 'format': 'json' },
      { 'jsoncallback': 'flickrcb' },
      { 'tags': 'dogs in hats' }
    ]))
  })
})
