import Vue from 'vue'

import Routes from './router.js'

import Footer from '@/modules/footer'

import './index.less'

// Bootstraps Vue components to the HTML
/* eslint-disable no-new */
new Vue({
  el: '#root',
  router: Routes,
  components: {
    footer: Footer
  },
  render: (vc) => vc('main', {}, [
    vc('router-view'),
    vc('footer')
  ])
})
