/* global fetch btoa */

import './image-card.less'

export default {
  name: 'image-card',
  data () {
    return {
      loaded: true,
      imageMain: '',
      imageLarge: ''
    }
  },
  props: [
    'item'
  ],
  methods: {
    fetchData (url) {
      fetch(url)
        .then(resp => resp.json())
        .then(json => {
          this.loaded = false
          this.fetchEncodedImage(json[json.length - 2]) // TEMP Get images by size
            .then(img => {
              this.imageMain = img
              this.loaded = true
            })
        })
    },
    fetchEncodedImage (url) {
      const ext = url.split('.').slice(-1).pop()

      if (!['jpg', 'png', 'tif', 'tiff'].includes(ext)) throw new Error('Url is not an image link')

      return fetch(url)
        .then(resp => resp.arrayBuffer())
        .then(img => [...new Uint8Array(img)] // Returns image Base64
          .reduce((str, buff) => {
            str += String.fromCharCode(buff)
            return str
          }, '')
        ).then(enc => `data:image/${ext};base64,${btoa(enc)}`) // Generates Url Data
    }
  },
  mounted () { // Loads images on mount
    this.fetchData(this.item.href)

    // TODO Sequencially load larger images
  },
  watch: {
    item (nxt) {
      return this.fetchData(nxt.href)
    }
  },
  render (vc) {
    return vc('article', {}, [
      vc('div', {
        attrs: {
          id: 'image-card'
        },
        class: this.loaded ? 'image-show' : 'image-hide',
        style: {
          'background-image': `url(${this.imageMain})`
        }
      })
    ])
  }
}
