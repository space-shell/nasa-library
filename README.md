# nasa-library

A web application that retreives and displays images and text from NASA's online library.

[![pipeline status](https://gitlab.com/spaceshell_j/nasa-library/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/nasa-library/commits/master)

[Demo - :link:](http://nasa-library.s3-website-eu-west-1.amazonaws.com/)

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Properties](#Properties)
* [Methods](#Methods)
* [Methods](#Methods)
* [Style](#Methods)
* [Testing](#Testing)
* [Authors](#Authors)

## Installation

To install, you can use [npm](https://npmjs.org/)

```bash
$ npm install component
```


## Usage

Import the component with the following lines.

```JavaScript
import Component from 'component'
```

Example:

```JavaScript

```


> Caveat

### Properties

  |Name|Description|
  |-|-|
  |||

### Methods

Component methods

---

#### Method One

Desc

```JavaScript
```

Properties:

  |Name|Description|
  |-|-|
  |||

---

## Style


## Testing

Unit test are run using Jest

```bash
$npm run test
```

## Authors

*James Nicholls*
